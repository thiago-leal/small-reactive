import { factory, getAllAttributesFrom, randomString } from "../utils/functions.js";
import { VirtualDom } from "../utils/virtual-dom.js";
import { Injector } from "../utils/injector.js";

import { Style } from "./style.js";
import { Module } from "./module.js";
import { Directive } from "./directive.js";


export class Component extends Injector {
  /**
   * @type { Record<string, any> }
   */
  #properties = {};
  /**
   * @type { HTMLElement | undefined }
   */
  #element = void 0;
  /**
   * @type { {
   *  selector: string
   *  component: Component
   *    | (new (...args: any[]) => Component)
   *    | ((...args: any[]) => Component)
   *  instances: Component[]
   * }[] }
   */
  #componentChildren = [];
  /**
   * @type { { selector: string, directive: Directive }[] }
  */
  #directiveChildren = [];
  /**
   * @type { string }
   */
  #id = "";
  /**
   * @type { HTMLElement | undefined }
   */
  #content = void 0;
  /**
   * @type { string[] }
   */
  #styles = [];
  /**
   * @type { string[] }
   */
  #deepStyles = [];
  /**
   * @type { Component | undefined }
   */
  #parent = undefined;
  /**
   * @type { Component }
   */
  #context = this;
  /**
   * @type { Map<HTMLElement, Record<string, Function[]>> }
   */
  #eventsMap = new Map();
  /**
   * @type { Map<Component, Record<string, Function[]>> }
   */
  #bindsMap = new Map();
  #initiated = false;
  #selector = "";
  #vDom = new VirtualDom();

  get element() {
    return this.#element;
  }

  get content() {
    return this.#content;
  }

  get parent() {
    return this.#parent;
  }

  #initProperties(props) {
    Object.keys(this).forEach(key => {
      props[key] = this[key];
      delete this[key];
    });
    if (props && typeof props === "object") {
      for (const key in props) {
        this.#properties[key] = props[key];
        Object.defineProperty(this, key, {
          get: () => this.#properties[key],
          set: value => {
            this.#properties[key] = value;
            this.reload();
          }
        });
      }
    }
  }

  #isToIgnore(element) {
    return this.#componentChildren
      .map(e => e.selector)
      .some(e => element.matches(e));
  }

  #assignComponent(element) {
    if (element instanceof HTMLElement) {
      if (!this.#isToIgnore(element)) {
        element.componentInstance = this;
        element.childNodes
          .forEach(child => this.#assignComponent(child));
      }
    }
  }

  #createStyle(style, deep) {
    const options = [
      { prefix: "", posfix: this.#selector },
      ...( deep ? [ { prefix: this.#selector + " ", posfix: "" } ] : [] )
    ];
    return Style.create(style, options, { component: this.#id });
  }

  /**
   * @param { HTMLElement } element 
   * @param { Component | (new (...a: any[]) => Component) | ((...a: any[]) => Component) } seed 
   * @param  { ...any } args 
   * @returns { Component | null }
   */
  #instanceComponent(element, seed, ...args) {
    let instance = seed;

    if (element && element.componentInstance) {
      if (element.componentInstance instanceof Component) {
        return element.componentInstance;
      }
    }
    instance = factory(seed, ...args);

    if (instance instanceof Component) {
      instance.#parent = this;
      return instance;
    }
    return null;
  }

  #initListeners(element) {
    if (!(element instanceof HTMLElement)) return;

    if (element.getAttribute("component") === this.#id) {
      const attributes = getAllAttributesFrom(element);
      const prefix = "event:";
      let elementListeners = this.#eventsMap.get(element);

      if (!elementListeners) {
        elementListeners = {};
        this.#eventsMap.set(element, elementListeners);
      }
      for (const key in attributes) {
        if (key.startsWith(prefix)) {
          const event = key.substring(prefix.length);
          const elementListenersForEvent = elementListeners[event] || [];

          elementListenersForEvent.forEach(listener => {
            element.removeEventListener(event, listener);
          });
          elementListenersForEvent.splice(0);
          elementListeners[event] = elementListenersForEvent;

          const listener = event => {
            new Function("event", "element", attributes[key])
              .call(this.#context, event, element);
          };
          elementListenersForEvent.push(listener);
          element.addEventListener(event, listener);
        }
      }
      if (element.componentInstance) {
        const prefix = "bind:";

        for (const key in attributes) {
          if (key.startsWith(prefix)) {
            if (!this.#bindsMap.has(element.componentInstance)) {
              this.#bindsMap.set(element.componentInstance, {});
            }
            const map = this.#bindsMap.get(element.componentInstance);

            const property = key.substring(prefix.length);
            map[property] = () => new Function(`return ${attributes[key]}`)
              .call(this.#context);
          }
        }
      }
    }
    element.childNodes.forEach(e => this.#initListeners(e));
  }

  constructor(options) {
    super(() => Module.getFromComponent(this.constructor));

    this.#id = [
      this.constructor.name.slice(0, 4),
      randomString(10)
    ].join("_");
    this.#selector = `[component=${ this.#id }]`;

    if (options) {
      if (options.children) {
        this.setChildren(options.children);
      }
      if (options.directives) {
        this.setDirectives(options.directives);
      }
      if (options.style) {
        if (!Array.isArray(options.style)) {
          options.style = [options.style];
        }
        options.style.forEach(style => this.useStyle(style));
      }
      if (options.deepStyle) {
        if (!Array.isArray(options.deepStyle)) {
          options.deepStyle = [options.deepStyle];
        }
        options.deepStyle.forEach(deepStyle => this.useDeepStyle(deepStyle));
      }
    }
  }

  render() { return ""; }
  onShow() { }
  onReload() { }
  onConnect() { }
  onDisconnect() { }

  getElementByRef(ref) {
    return this.element.querySelector(`[ref="${ref}"]`);
  }

  getElementsByRef(ref) {
    return Array.from(this.element.querySelectorAll(`[ref="${ref}"]`));
  }

  getComponentByRef(ref) {
    const element = this.getElementByRef(ref)
    return element ? element.componentInstance : void 0;
  }

  getComponentsByRef(ref) {
    return this.getElementsByRef(ref)
      .filter(e => !!e.componentInstance)
      .map(e => e.componentInstance)
  }

  useStyle(style) {
    this.#styles.push(style);
  }

  useDeepStyle(style) {
    this.#deepStyles.push(style);
  }

  appendChild(selector, component) {
    this.#componentChildren.push({ component, selector, instances: [] });
  }

  appendDirective(selector, directive) {
    directive = factory(directive);

    if (directive instanceof Directive) {
      directive.setComponent(this);
      this.#directiveChildren.push({ directive, selector });
    }
  }

  setChildren(children) {
    if (typeof children === "object") {
      if (Array.isArray(children)) {
        children.forEach(e => this.appendChild(e.selector, e.componentInstance));
      } else {
        for (const key in children) {
          this.appendChild(key, children[key]);
        }
      }
    }
  }

  setDirectives(directive) {
    if (typeof directive === "object") {
      if (Array.isArray(directive)) {
        directive.forEach(e => this.appendDirective(e.selector, e.directive));
      } else {
        for (const key in directive) {
          this.appendDirective(key, directive[key]);
        }
      }
    }
  }

  setContext(context) {
    if (context instanceof Component) {
      this.#context = context;
    }
  }

  eventEmitter(event) {
    return {
      emit: data => {
        this.emit(event, data);
      }
    };
  }

  emit(event, data) {
    if (!data) data = {};
    if (!data.detail) {
      data = { detail: data };
    }
    this.element.dispatchEvent(new CustomEvent(event, data))
  }

  destroy(disconnect) {
    this.#componentChildren.forEach(({ instances }) => {
      instances.forEach(instance => {
        instance.destroy(disconnect);
      });
      instances.splice(0);
    });

    if (disconnect) {
      document.head
        .querySelectorAll(`style${ this.#selector }`)
        .forEach(e => e.remove());

      this.onDisconnect();
      this.#eventsMap.forEach((events, element) => {
        for (const event in events) {
          const listeners = events[event];
          listeners.forEach(listener => {
            element.removeEventListener(event, listener);
          });
          this.#eventsMap.clear();
        }
      });
    }
    if (this.#initiated) {
      this.#bindsMap.clear();
    }
  }

  showComponentInElement(element) {
    this.destroy();
    this.#initProperties({});
    this.#element = element;
    this.#initiated = true;
    this.reload();
    if (!element.componentInstance) {
      this.#assignComponent(element);
      this.#styles.forEach(style => this.#createStyle(style))
      this.#deepStyles.forEach(style => this.#createStyle(style, true));
      this.onConnect();
    }
    this.#initListeners(element);
    this.onShow();
  }

  reload() {
    if (!this.element) return;

    const template = this.render(this.element);

    this.#vDom.load(template);
    this.#vDom.ignore = this.#componentChildren.map(e => e.selector);

    const changes = this.#vDom.apply(this.element, { component: this.#id });

    if (changes) {
      const children = [];

      this.#componentChildren.forEach(({ component, instances, selector }) => {
        const used = [], remove = [];
        const elements = this.#vDom.template.querySelectorAll(selector);

        this.element.querySelectorAll(selector).forEach((element, index) => {
          const instance = this.#instanceComponent(element, component);

          if (instance) {
            if (!instances.includes(instance)) {
              instances.push(instance);
            }
            instance.#content = elements[index];
            instance.showComponentInElement(element);
            element.componentInstance = instance;
            children.push({
              element, component: instance
            });
            used.push(instance);
          }
        });
        instances.forEach((instance, index) => {
          if (!used.includes(instance)) {
            remove.unshift(index);
            instance.destroy(true);
          }
        });
        remove.forEach(i => instances.splice(i, 1));
      });
      this.#directiveChildren.forEach(({ directive, selector }) => {
        this.element.querySelectorAll(`[${selector}]`).forEach(element => {
          directive.init(element, selector);
        });
      });
      this.element.childNodes.forEach(e => this.#initListeners(e));
      this.#bindsMap.forEach((properties, component) => {
        for (const key in properties) {
          component[key] = properties[key].call();
        }
      });
      this.onReload();
    }
  }
}
