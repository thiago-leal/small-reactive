import { Module } from "../../core.js";
import { Injector } from "../utils/injector.js";

export class Directive extends Injector {
  #component = null;

  constructor() {
    super(() => Module.getFromDirective(this.constructor));
  }

  setComponent(component) {
    this.#component = component;
  }

  get component() {
    return this.#component;
  }

  apply() {}

  init(element, selector) {
    if (element instanceof HTMLElement && typeof selector === "string") {
      const value = element.getAttribute(selector);
      this.apply(element, value, this.component);
    }
  }
}
