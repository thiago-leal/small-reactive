import { Subject } from "../../rx.js";
import { Exception } from "../exceptions/exception.js";
import { Injector } from "../utils/injector.js";
import { Module } from "./module.js";

export class Service extends Injector {
  #module = null;

  static currentContext = null;

  constructor() {
    super(() => this.#module);

    const context = this.constructor.currentContext || {};

    if (!(context.module instanceof Module)) {
      throw new Exception("Invalid module");
    }
    this.#module = context.module;
    this.events$ = new Subject();

    this.notify({
      event: "create",
      instance: this,
      context
    });
  }

  onRegister() { }

  onImport() { }

  onGet() { }

  notify(event) {
    this.events$.next(event);
  }

  events() {
    return this.events$;
  }
}
