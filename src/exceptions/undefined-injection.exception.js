import { Exception } from "./exception.js";

export class UndefinedInjectionException extends Exception {
  constructor(inject) {
    inject = typeof inject === "string"
      ? inject
      : inject && inject.name
        ? inject.name
        : inject && inject.constructor && inject.constructor.name
          ? inject.constructor.name
          : inject && typeof inject.toString === "function"
            ? inject.toString()
            : JSON.stringify(inject);
    super(`Undefined injection for ${ inject }`);
  }
}
