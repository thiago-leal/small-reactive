import { Exception } from "./exception.js";

export class UndefinedModuleException extends Exception {
  constructor(className) {
    super(`Undefined module for ${ className }`);
  }
}
