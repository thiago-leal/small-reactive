import { Subscription, fromEventTarget } from "../../../rx.js";
import { FormFieldComponent } from "./form-field.component.js";
import { mapAttributes } from "./utils.js";

export class BaseField extends FormFieldComponent {
  #subscription = null;
  #invalidClass = [];
  #validClass = [];

  errorMessage = "";
  defaultInvalidClassName = "invalid";
  defaultValidClassName = "valid";

  constructor() {
    super();
  }

  onConnect() {
    super.onConnect();
    this.#subscription = new Subscription();
    const invalidClass = this.element.getAttribute("invalid-class") || this.defaultInvalidClassName;
    const validClass = this.element.getAttribute("valid-class") || this.defaultValidClassName;

    this.#invalidClass = invalidClass.split(" ").filter(e => !!e);
    this.#validClass = validClass.split(" ").filter(e => !!e);

    const element = this.getElement();

    if (element) {
      this.#subscription.add(
        fromEventTarget(element, "blur")
          .subscribe(() => {
            const controller = this.getController();

            if (controller) {
              controller.markAsTouched();
            }
          })
      );
    }
  }

  onDisconnect() {
    super.onDisconnect();
    this.#subscription.unsubscribe();
  }

  getControllerName() {
    return this.element.getAttribute("controller");
  }

  getElement() {
    return null;
  }

  setValue() {}

  initValue(value) {
    this.setValue(value);
  }

  onSetValue(value) {
    this.setValue(value);
  }

  onSetValidate(valid) {
    const element = this.getElement();

    if (element) {
      const controller = this.getController();

      if (valid) {
        this.#invalidClass.forEach(e => {
          element.classList.remove(e);
        });
        this.#validClass.forEach(e => {
          element.classList.add(e);
        });
      } else {
        this.#validClass.forEach(e => {
          element.classList.remove(e);
        });
        this.#invalidClass.forEach(e => {
          element.classList.add(e);
        });
      }
      if (controller) {
        this.errorMessage = controller.error;
      }
    }
  }

  getValueObserver() {
    return from("");
  }

  renderField() {
    return "";
  }

  render() {
    const error = this.content
      ? this.content.querySelector("field-error, [field-error]")
      : void 0;
    const errorAttrMap = mapAttributes(error);

    return /*html*/`
      ${ this.renderField() }
      ${
        error ? /*html*/`<${ error.tagName } ${ errorAttrMap.join(" ") }>
          ${ this.errorMessage }
        </${ error.tagName }>` : ""
      }
    `;
  }
}
