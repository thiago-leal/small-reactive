import { Subject, Subscription } from "../../../rx.js";

export class Controller {
  #value = null;
  #initial = null;
  #touched = false;
  #dirty = false;
  #valid = false;
  #error = "";
  #validators = [];

  #valueChanges$ = new Subject();
  #statusChanges$ = new Subject();
  #subscription = null;

  #get = e => e;
  #set = e => e;
  #setValueCallback = () => void 0;
  #validateCallback = () => void 0;

  get value() {
    return this.#get(this.#value);
  }

  get valid() {
    return this.#valid;
  }

  get touched() {
    return this.#touched;
  }

  get dirty() {
    return this.#dirty;
  }

  get error() {
    return this.#error;
  }

  get valueChanges() {
    return this.#valueChanges$;
  }

  get statusChanges() {
    return this.#statusChanges$;
  }

  constructor(options) {
    if (!options) options = {};

    this.#initial = this.#value = options.value !== void 0 ? options.value : null;
    if (typeof options.get === "function") this.#get = options.get;
    if (typeof options.set === "function") this.#set = options.set;
    this.setValidators(options.validators);
  }

  connect(setValueCallback, validateCallback, observer$) {
    this.#subscription = new Subscription();
    this.#setValueCallback = setValueCallback;
    this.#validateCallback = validateCallback;
    this.#subscription.add(observer$.subscribe(value => this.setValue(value)));
  }

  disconnect() {
    this.#subscription.unsubscribe();
    this.#setValueCallback = () => void 0;
    this.#validateCallback = () => void 0;
  }

  async validate() {
    if (this.touched) {
      let error = "", valid = true;
      this.#valid = false;
      this.#error = "";

      await Promise.all(this.#validators.map(async validator => {
        const err = await validator(this.value);

        if (typeof err === "string" && !error) {
          error = err;
        }
        valid &&= !err;
      }));
      this.#error = error;
      this.#valid = valid;
    } else if (this.#error || !this.#valid) {
      this.#error = "";
      this.#valid = true;
    }
    this.statusChanges.next(this.valid);
    this.#validateCallback(this.valid);
  }

  setValidators(validators) {
    if (typeof validators === "function") {
      this.#validators = [ validators ];
    } else if (Array.isArray(validators)) {
      this.#validators = validators.filter(e => typeof e === "function");
    } else {
      this.#validators = [];
    }
  }

  setValue(value) {
    this.#value = this.#set(value);
    this.validate().then(() => {
      this.valueChanges.next(this.value);
      this.#setValueCallback(this.#value);
    });
  }

  markAsTouched() {
    this.#touched = true;
    this.validate();
  }

  markAsUntouched() {
    this.#touched = false;
    this.#error = "";
    this.#validateCallback(this.#valid = true);
  }

  markAsDirty() {
    this.#dirty = true;

    if (this.touched) {
      this.validate();
    }
  }

  markAsPristine() {
    this.#dirty = false;
    this.markAsUntouched();
  }

  reset() {
    this.markAsPristine();
    this.setValue(this.#initial);
  }
}
