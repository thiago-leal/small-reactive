import { Component } from "../../../core.js";
import { Controller } from "./controller.class.js";
import { FormComponent } from "./form.component.js";

export class FormFieldComponent extends Component {
  /**
   * @type { Controller | undefined }
   */
  #controller = void 0;

  constructor(options) {
    super(options);
  }

  onConnect() {
    this.#controller = this.getController();

    if (this.#controller) {
      this.#controller.connect(
        value => this.onSetValue(value),
        valid => this.onSetValidate(valid),
        this.getValueObserver()
      );
      this.initValue(this.#controller.value);
    }
  }

  onDisconnect() {
    if (this.#controller) {
      this.#controller.disconnect();
    }
    this.#controller = void 0;
  }

  getController() {
    if (this.parent instanceof FormComponent) {
      return this.parent.getController(this.getControllerName());
    }
  }

  getControllerName() { return ""; }
  initValue() {}
  onSetValue() {}
  onSetValidate() {}
  getValueObserver() {}
}
