import { Component } from "../../../core.js";
import { Controller } from "./controller.class.js";

export class FormComponent extends Component {
  #controllers = {};

  get controllers() {
    return this.#controllers;
  }

  #getAllControllers() {
    const controllers = [];

    for (const key in this.#controllers) {
      controllers.push(this.getController(key));
    }
    return controllers;
  }

  constructor(options) {
    super(options);

    this.setControllers((options ? options.controllers : void 0) || {});
  }

  setControllers(controllersOptions) {
    const controllers = {};

    for (const key in controllersOptions) {
      controllers[key] = new Controller(controllersOptions[key])
    }
    this.#controllers = controllers;
  }

  getController(name) {
    return this.#controllers[name];
  }

  async validateField(name) {
    const controller = this.getController(name);

    if (controller instanceof Controller) {
      await controller.validate();
      return controller.valid;
    }
    return false;
  }

  async validate() {
    let valid = true;

    for (const key in this.#controllers) {
      valid &&= await this.validateField(key);
    }
    return valid;
  }

  markAllAsTouched() {
    this.#getAllControllers().forEach(e => e.markAsTouched());
  }

  markAllAsUntouched() {
    this.#getAllControllers().forEach(e => e.markAsUntouched());
  }

  clear() {
    this.#getAllControllers().forEach(e => e.setValue(null));
  }

  reset() {
    this.#getAllControllers().forEach(e => e.reset());
  }
}
