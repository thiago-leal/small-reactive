import { fromEventTarget, map } from "../../../rx.js";
import { BaseField } from "./base-field.component.js";
import { mapAttributes } from "./utils.js";

export class InputField extends BaseField {
  constructor() {
    super();
  }

  getElement() {
    return this.getElementByRef("field");
  }

  setValue(value) {
    const element = this.getElement();

    if (element) {
      element.value = value;
    }
  }

  getValueObserver() {
    const element = this.getElement();

    if (element) {
      return fromEventTarget(element, "input")
        .pipe(map(() => element.value))
    }
    return from("");
  }

  renderField() {
    const input = this.content ? this.content.querySelector("input") : void 0;
    const textarea = this.content ? this.content.querySelector("textarea") : void 0;
    const fieldAttrMap = mapAttributes(input || textarea);

    return /*html*/`
      ${ input || !textarea ? /*html*/`<input ref="field" ${ fieldAttrMap.join(" ") }>` : "" }
      ${ textarea ? /*html*/`<textarea ref="field" ${ fieldAttrMap.join(" ") }></textarea>` : "" }
    `;
  }
}
