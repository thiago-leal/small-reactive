import { fromEventTarget, map } from "../../../rx.js";
import { BaseField } from "./base-field.component.js";
import { mapAttributes } from "./utils.js";

export class SelectField extends BaseField {
  constructor() {
    super();
  }

  getElement() {
    return this.getElementByRef("field");
  }

  setValue(value) {
    const element = this.getElement();

    if (element) {
      element.value = value;
    }
  }

  getValueObserver() {
    const element = this.getElement();

    if (element) {
      return fromEventTarget(element, "change")
        .pipe(map(() => element.value))
    }
    return from("");
  }

  renderField() {
    const select = this.content ? this.content.querySelector("select") : void 0;
    const fieldAttrMap = mapAttributes(select);

    return /*html*/`
      <select ref="field" ${ fieldAttrMap.join(" ") }>
        ${ select ? select.innerHTML : "" }
      </select>
    `;
  }
}
