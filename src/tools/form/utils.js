export function mapAttributes(element) {
  const attr = Array.from((element || {}).attributes || []);
  return attr.map(
    e => [ e.nodeName, e.nodeValue ].map((e, i) => !i ? `${ e }` : `"${ e }"`).join("=")
  );
}
