export const validators = Object.freeze({
  required: message => {
    return value => value && value.length > 0 ? "" : (message || "Required field");
  },
  minLength: (length, message) => {
    return value => !value || value.length >= length ? "" : (message || "Field too short");
  },
  maxLength: (length, message) => {
    return value => !value || value.length <= length ? "" : (message || "Field too long");
  },
  min: (value, message) => {
    return field => field >= value ? "" : (message || "Value too short");
  },
  max: (value, message) => {
    return field => field <= value ? "" : (message || "Value too long");
  },
  in: (values, message) => {
    return field => values.includes(field) ? "" : (message || "Invalid field value");
  },
  pattern: (regex, message) => {
    return field => regex.test(field) ? "" : (message || "Invalid field value");
  }
});
