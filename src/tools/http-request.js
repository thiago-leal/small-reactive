import { Service } from "../../core.js";
import { Observable } from "../../rx.js";
import { RegistryManager } from "../utils/registry-manager.js";

export class HttpRequest extends Service {
  #beforeSendCallbacks = new RegistryManager();
  #afterSendCallbacks = new RegistryManager();

  constructor() {
    super();
  }

  registerBeforeSendCallback(callback) {
    return this.#beforeSendCallbacks.registry(callback);
  }

  registerAfterSendCallback(callback) {
    return this.#afterSendCallbacks.registry(callback);
  }

  async request(url, method, body, opts) {
    const before = this.#beforeSendCallbacks.getAll();
    const after = this.#afterSendCallbacks.getAll();

    for (let i = 0; i < before.length; i++) {
      const callback = before[i];
      const callbackReturn = await callback({ url, method, body, opts });

      if (callbackReturn) {
        if (callbackReturn.url) url = callbackReturn.url;
        if (callbackReturn.method) method = callbackReturn.method;
        if (callbackReturn.body !== void 0) {
          body = callbackReturn.body ? callbackReturn.body : void 0;
        }
        if (callbackReturn.opts !== void 0) {
          opts = callbackReturn.opts ? callbackReturn.opts : void 0;
        }
      }
    }
    if (opts === null || opts === undefined || typeof opts !== "object") {
      opts = {};
    }
    opts.method = method;

    if (!opts.headers) {
      opts.headers = {};
    }
    if (body) {
      if (opts.json) {
        opts.body = JSON.stringify(body);
        opts.headers = { "content-type": "application/json", ...opts.headers };
      } else {
        opts.body = body;
      }
    }
    let ret = await fetch(url, opts);

    for (let i = 0; i < after.length; i++) {
      const callback = after[i];
      ret = await callback(ret);
    }
    return ret;
  }

  async parseResponseValue(response, cb) {
    if (response && response.ok) {
      return cb ? cb(response) : response;
    }
    throw new Error(response ? response.statusText : "Request failed");
  }

  async get(url, args) {
    const cb = (args || {}).transform;
    const response = await this.request(url, "GET", null, args);
    return this.parseResponseValue(response, cb);
  }

  async post(url, body, args) {
    const cb = (args || {}).transform;
    const response = await this.request(url, "POST", body, args);
    return this.parseResponseValue(response, cb);
  }

  async put(url, body, args) {
    const cb = (args || {}).transform;
    const response = await this.request(url, "PUT", body, args);
    return this.parseResponseValue(response, cb);
  }

  async patch(url, body, args) {
    const cb = (args || {}).transform;
    const response = await this.request(url, "PATCH", body, args);
    return this.parseResponseValue(response, cb);
  }

  async delete(url, args) {
    const cb = (args || {}).transform;
    const response = await this.request(url, "DELETE", null, args);
    return this.parseResponseValue(response, cb);
  }

  /**
   * @param {ReadableStreamDefaultReader<Uint8Array>} reader 
   * @param {(value: Uint8Array) => void} onData 
   */
  async #readFromStream(reader, onData) {
    const readChunk = async () => {
      const { done, value } = await reader.read();

      if (done) return false;

      onData(value);
      return true;
    };

    while (await readChunk());
  }

  observeStream(url, method, body, opts) {
    return new Observable(async observe => {
      try {
        const response = await this.request(url, method, body, opts);
        const reader = response.body.getReader();

        this.#readFromStream(reader, e => observe.next(e))
          .then(() => {
            observe.complete();
          })
          .catch(e => {
            observe.error(e);
          });

        return () => reader.cancel();
      } catch (e) {
        observe.error(e);
      }
    });
  }

  getStream(url, args) {
    return this.observeStream(url, "GET", null, args);
  }

  postStream(url, body, args) {
    return this.observeStream(url, "POST", body, args);
  }

  putStream(url, body, args) {
    return this.observeStream(url, "PUT", body, args);
  }

  patchStream(url, body, args) {
    return this.observeStream(url, "PATCH", body, args);
  }

  deleteStream(url, args) {
    return this.observeStream(url, "DELETE", null, args);
  }
}
