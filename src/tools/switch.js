import { getAllAttributesFrom } from "../utils/functions.js";
import { Component } from "../../core.js";

export class Switch extends Component {
  #components = {};
  #selectedKey = void 0;

  get selected() {
    const item = this.element.children.item(0);
    return item ? item.component : void 0;
  }

  get keys() {
    return Object.keys(this.#components);
  }

  #getSelectorFromKey(key) {
    return `switch-${ key.replace(/[^A-Za-z0-9]/gi, "-").replace(/^\-/, "") }`;
  }

  constructor(options) {
    super(options);
  }

  onConnect() {
    if (this.parent) {
      this.setContext(this.parent);
    }
  }

  setComponent(key, component) {
    this.#components[key] = component;
    this.appendChild(this.#getSelectorFromKey(key), component);
  }

  select(key) {
    if (this.keys.includes(key) && key !== this.#selectedKey) {
      this.#selectedKey = key;
    }
    this.reload();
  }

  render() {
    const key = this.#selectedKey;

    if (key) {
      const selector = this.#getSelectorFromKey(key);
      const allAttributes = getAllAttributesFrom(this.element);
      const attributes = Object.keys(allAttributes)
        .map(key => `${ key }="${ allAttributes[key] }"`)
        .join(" ");

      return `<${ selector } ${ attributes }></${ selector }>`;
    }
    return "";
  }
}
