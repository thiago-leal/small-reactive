export function functionType(value) {
	return typeof value === "function"
		? value.prototype
			? Object.getOwnPropertyDescriptor(value, "prototype").writable
				? "function"
				: "class"
			: value.constructor.name === "AsyncFunction"
				? "async"
				: "arrow"
		: null;
}

export function parseHTML(str) {
	const parser = new DOMParser();
	const doc = parser.parseFromString(str, "text/html");
	return doc.body;
}

export function getAllAttributesFrom(element) {
	const attributes = {};
	Array.from(element.attributes).forEach(attr => {
		attributes[attr.nodeName] = attr.nodeValue;
	});
	return attributes;
}

export function randomString(length, chars) {
	if (chars === undefined) {
		chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	}
	let ret = "";
	for (let i = 0; i < length; i++) {
		ret += chars.charAt(Math.floor(Math.random() * chars.length));
	}
	return ret;
}

export function factory(seed, ...args) {
	switch (functionType(seed)) {
		case "class":
			return new seed(...args);
		case "function":
		case "arrow":
		case "async":
			return seed(...args);
		case null:
			return seed;
	}
}

export function diff(a, b) {
	if (a === b) return false;
	if (typeof a !== typeof b) return true;
	if (a && !b || !a && b) return true;
	if (typeof a === "object" && a && b) {
		if (Array.isArray(a) && Array.isArray(b)) {
			if (a.length !== b.length) return true;
			for (let i = 0; i < a.length; i++) {
				if (diff(a[i], b[i])) return true;
			}
		} else if (Array.isArray(a) || Array.isArray(b)) {
			return true;
		} else {
			for (const key in a) {
				if (diff(a[key], b[key])) return true;
			}
		}
	}
	return a !== b;
}
