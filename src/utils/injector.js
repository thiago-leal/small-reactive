import { UndefinedModuleException } from "../exceptions/undefined-module.exception.js";

export class Injector {
  #getModule = () => null;

  constructor(getModule) {
    if (typeof getModule === "function") {
      this.#getModule = getModule;
    }
  }

  inject(service, options) {
    const module = this.#getModule();

    if (module) {
      return module.inject(service, options);
    }
    throw new UndefinedModuleException(this.constructor.name);
  }
}
