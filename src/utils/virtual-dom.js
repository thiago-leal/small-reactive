import { getAllAttributesFrom, parseHTML } from "./functions.js";

function getNodeType(node) {
	if (node.nodeType === 3) return "text";
	if (node.nodeType === 8) return "comment";
	return node.tagName.toLowerCase();
}

function getNodeContent(node) {
	if (node.childNodes && node.childNodes.length > 0) return null;
	return node.textContent;
}

function diffAttributes(template, element) {
	const templateAttributes = getAllAttributesFrom(template);
	const elementAttributes = getAllAttributesFrom(element);
	let changes = 0;

	for (const key in elementAttributes) {
		if (!template.getAttribute(key)) {
			element.removeAttribute(key);
			changes++;
		}
	}
	for (const key in templateAttributes) {
		if (template.getAttribute(key) !== element.getAttribute(key)) {
			element.setAttribute(key, template.getAttribute(key));
			changes++;
		}
	}
	return changes;
}

/**
 * @param {HTMLElement} template 
 * @param {HTMLElement} element 
 * @param {string[]} ignore 
 * @returns {number}
 */
function applyDiff(template, element, ignore) {
	const domNodes = Array.from(element ? element.childNodes : []);
	const templateNodes = Array.from(template.childNodes);
	let count = domNodes.length - templateNodes.length;
	let changes = 0;

	if (count > 0) {
		for (let i = 1; i <= count; i++) {
			const child = domNodes[domNodes.length - i];
			child.parentNode.removeChild(child);
			changes++;
		}
	}
	templateNodes.forEach((node, index) => {
		if (!domNodes[index]) {
			const child = node.cloneNode(true);
			element ? element.appendChild(child) : void 0;
			changes++;
			return;
		}
		if (getNodeType(node) !== getNodeType(domNodes[index])) {
			const child = node.cloneNode(true);
			const current = domNodes[index];
			domNodes[index].parentNode.replaceChild(child, current);
			changes++;
			return;
		}
		if (node instanceof HTMLElement) {
			if (ignore.some(i => node.matches(i))) {
				changes += (diffAttributes(node, domNodes[index]) ? 1 : 0);
				changes += (applyDiff(node, domNodes[index].cloneNode(true), []) ? 1 : 0);
				return;
			}
		}
		const templateContent = getNodeContent(node);
		if (templateContent && templateContent !== getNodeContent(domNodes[index])) {
			domNodes[index].textContent = templateContent;
			changes++;
		}
		if (domNodes[index].childNodes.length > 0 && node.childNodes.length < 1) {
			domNodes[index].innerHTML = "";
			changes++;
			return;
		}
		if (domNodes[index].childNodes.length < 1 && node.childNodes.length > 0) {
			const fragment = document.createDocumentFragment();
			changes += applyDiff(node, fragment, ignore);
			domNodes[index].appendChild(fragment);
			changes++;
			return;
		}
		if (node.childNodes.length > 0) {
			changes += applyDiff(node, domNodes[index], ignore);
		}
		if (node instanceof HTMLElement && domNodes[index] instanceof HTMLElement) {
			changes += (diffAttributes(node, domNodes[index]) ? 1 : 0);
		}
	});
	return changes;
}

export class VirtualDom {
	/**
	 * @type { HTMLElement | null }
	 */
	#template = null;
	/**
	 * @type { string[] }
	 */
	#ignore = [];

	get template() {
		return this.#template;
	}

	get ignore() {
		return this.#ignore;
	}
	set ignore(value) {
		if (Array.isArray(value) && value.every(e => typeof e === "string")) {
			this.#ignore = [ ...new Set(value) ];
		}
	}

	load(template) {
		this.#template = parseHTML(template);
	}

	#assignAttributes(attributes, element, options) {
		if (element instanceof HTMLElement) {
			for (let attr in attributes) {
				if (options.force || !element.hasAttribute(attr)) {
					element.setAttribute(attr, attributes[attr]);
				}
			}
			element.childNodes.forEach(e => this.#assignAttributes(attributes, e, options));
		}
	}

	apply(element, attributes, options) {
		this.#assignAttributes(attributes, this.#template, options || {});
		return applyDiff(this.#template, element, this.#ignore);
	}
}
