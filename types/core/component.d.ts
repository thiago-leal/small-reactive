import { Directive } from "./directive";
import { Constructable } from "../utils/constructable";
import { Injector } from "../utils/injector";

export type ChildrenDefinitionObject<T> = { selector: string, component: T };
export type DirectivesDefinitionObject<T> = { selector: string, directive: T };
export type ChildrenRecordOrArray<T> = Record<string, T> | ChildrenDefinitionObject<T>[]
export type DirectivesRecordOrArray<T> = Record<string, T> | DirectivesDefinitionObject<T>[]

export type ComponentOptions = {
  children?:    ChildrenRecordOrArray<Component | Constructable<Component> | (() => Component)>
  directives?:  DirectivesRecordOrArray<Directive | Constructable<Directive> | (() => Directive)>
  style?:       string | string[]
  deepStyle?:   string | string[]
};

export interface OnShow {
  onShow(): void;
}
export interface OnReload {
  onReload(): void;
}
export interface OnConnect {
  onConnect(): void;
}
export interface OnDisconnect {
  onDisconnect(): void;
}

export declare abstract class Component extends Injector {
  constructor(opts?: ComponentOptions);

  get element(): HTMLElement | undefined;
  get content(): HTMLElement | undefined;
  get parent(): Component | undefined;

  getElementByRef<T extends HTMLElement = HTMLElement>(ref: string): T | null;
  getElementsByRef<T extends HTMLElement = HTMLElement>(ref: string): T[];
  getComponentByRef<T extends Component>(ref: string): T | undefined;
  getComponentsByRef<T extends Component>(ref: string): T[];

  abstract render(): string;

  showComponentInElement(element: HTMLElement): void;
	useStyle(style: string): void;
	useDeepStyle(style: string): void;
  appendChild(
    selector: string,
    component: Component | Constructable<Component> | (() => Component)
  ): void;
  appendDirective(
    selector: string,
    directive: Directive | Constructable<Directive> | (() => Directive)
  ): void;
  setChildren(
    children: ChildrenRecordOrArray<Component | Constructable<Component> | (() => Component)>
  ): void;
  setDirectives(
    directives: DirectivesRecordOrArray<Directive | Constructable<Directive> | (() => Directive)>
  ): void;
  eventEmitter<T = any>(event: string): {
    emit: (data: T) => void
  };
  emit<T = any>(event: string, data: T): void;
  reload(): void;
}
