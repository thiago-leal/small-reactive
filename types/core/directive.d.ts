import { Injector } from "../utils/injector";
import { Component } from "./component";

export abstract class Directive extends Injector {
  constructor();
  get component(): Component | undefined;
  setComponent(component?: Component): void;
  init(element: HTMLElement, selector: string): void;
  abstract apply(element: HTMLElement, value: string, component: Component): void;
}
