import { Injector } from "../utils/injector";
import { Observable } from "../../rx";
import { Module } from "./module";

export interface OnRegister {
  onRegister(module: Module): void | Promise<void>;
}

export interface OnImport {
  onImport(module: Module): void | Promise<void>;
}

export interface OnGet {
  onGet(): void | Promise<void>;
}

export class Service<T = any> extends Injector {
  constructor();

  notify(event: T): void;
  events(): Observable<T>;
}
