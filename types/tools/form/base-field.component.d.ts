import { FormFieldComponent } from "./form-field.component";

export declare abstract class BaseField<T> extends FormFieldComponent<T> {
  protected defaultInvalidClassName: string;
  protected defaultValidClassName: string;

  override onConnect(): void;
  override onDisconnect(): void;

  override getControllerName(): string;
  override initValue(value: T): void;
  override onSetValue(value: T): void;
  override onSetValidate(valid: boolean): void;
  override render(): string;

  abstract getElement(): HTMLElement;
  abstract setValue(value: T): void;
  abstract renderField(): string;
}
