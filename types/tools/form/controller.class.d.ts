import { ValueOrArray, ValueOrPromise } from "../../types";
import { Observable } from "../../../rx";

export type ControllerOptions<T> = {
  value?:       T
  validators?:  ((value: T) => string) | ((value: T) => Promise<string>) |
    (((value: T) => string) | ((value: T) => Promise<string>))[]
  get?:         (value: any) => T
  set?:         (value: T) => any
};

export declare class Controller<T> {
  get value(): T
  get status(): boolean
  get touched(): boolean
  get dirty(): boolean
  get error(): string
  get valueChanges(): Observable<T>
  get statusChanges(): Observable<boolean>

  constructor(options: ControllerOptions<T>)

  connect(
    setValueCallback: (value: T) => void | Promise<void>,
    validateCallback: (valid: boolean) => void | Promise<void>,
    observer$: Observable<T>
  ): void
  disconnect(): void
  validate(): Promise<void>
  setValidators(validators: ValueOrPromise<ValueOrArray<(value: T) => string>>): void
  setValue(value: T): void
  markAsTouched(): void
  markAsUntouched(): void
  markAsDirty(): void
  markAsPristine(): void
  reset(): void
}
