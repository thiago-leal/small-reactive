import { Component, OnConnect, OnDisconnect } from "../../../core";
import { Observable } from "../../../rx";
import { Controller } from "./controller.class";

export declare abstract class FormFieldComponent<T> extends Component
  implements OnConnect, OnDisconnect
{
  onConnect(): void;
  onDisconnect(): void;

  getController(name: string): Controller<T>

  abstract getControllerName(): string;
  abstract initValue(value: T): void;
  abstract onSetValue(value: T): void;
  abstract onSetValidate(valid: boolean): void;
  abstract getValueObserver(): Observable<T>;
}
