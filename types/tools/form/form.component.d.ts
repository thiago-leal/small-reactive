import { Component, ComponentOptions } from "../../../core";
import { Controller, ControllerOptions } from "./controller.class";

export type MapType<T> = { readonly [K in keyof T]: T[K] };
export type MapController<T> = { readonly [K in keyof T]: Controller<T[K]> };
export type MapControllerOptions<T> = { readonly [K in keyof T]: ControllerOptions<T[K]> };

export type FormComponentOptions<T> = ComponentOptions & {
  controllers: MapControllerOptions<T>
}

export declare abstract class FormComponent<T extends object = any> extends Component {
  get controllers(): MapController<T>

  constructor(options?: FormComponentOptions<MapType<T>>)

  setControllers(controllersOptions: MapControllerOptions<T>): void
  getController<K extends keyof T, U = T[K]>(name: K): Controller<U>
  validateField(name: string): Promise<boolean>
  validate(): Promise<boolean>
  markAllAsTouched(): void
  markAllAsUntouched(): void
  clear(): void
  reset(): void
}
