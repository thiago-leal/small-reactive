import { Observable } from "../../../rx";
import { BaseField } from "./base-field.component";

export declare class InputField extends BaseField<string> {
  override getElement(): HTMLElement;
  override setValue(value: string): void;
  override getValueObserver(): Observable<string>;

  override renderField(): string;
}
