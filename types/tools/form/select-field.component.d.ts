import { Observable } from "../../../rx";
import { BaseField } from "./base-field.component";

export declare class SelectField<T = string> extends BaseField<T> {
  override getElement(): HTMLElement;
  override setValue(value: T): void;
  override getValueObserver(): Observable<T>;

  override renderField(): string;
}
