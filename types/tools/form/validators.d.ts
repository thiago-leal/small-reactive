export declare const validators: {
  readonly required:  (message?: string) => string,
  readonly minLength: (length: number, message?: string) => string,
  readonly maxLength: (length: number, message?: string) => string,
  readonly min:       (value: number, message?: string) => string,
  readonly max:       (value: number, message?: string) => string,
  readonly in:        <T>(values: T[], message?: string) => string,
  readonly pattern:   (regex: RegExp, message?: string) => string
};
