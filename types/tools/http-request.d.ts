import { Service } from "../../core"
import { Observable } from "../../rx";
import { RegistryManagerReturn } from "../utils/registry-manager"

type Transform<T> = { transform: (response: Response) => T | Promise<T> };

export type RequestInitJson = RequestInit & { json?: boolean };
export type RequestInitTransform<T> = RequestInit & Transform<T>;
export type RequestInitJsonTransform<T> = RequestInitJson & Transform<T>;

export type RequestData = {
  url?:     string
  method?:  string
  body?:    any
  opts?:    RequestInitJson
};

export declare class HttpRequest<E = any> extends Service<E> {
  registerBeforeSendCallback(
    callback: (args?: RequestData) => RequestData | undefined
  ): RegistryManagerReturn;

  registerAfterSendCallback(
    callback: (value: Response) => Response | Promise<Response>
  ): RegistryManagerReturn;

  request<T = any>(
    url: string,
    method: string,
    body?: any,
    opts?: RequestInitJson
  ): Promise<T>;

  get(url: string, args?: RequestInit): Promise<Response>;
  get<T>(url: string, args: RequestInitTransform<T>): Promise<T>;

  post(url: string, body: any, args?: RequestInitJson): Promise<Response>;
  post<T>(url: string, body: any, args?: RequestInitJsonTransform<T>): Promise<T>;

  put(url: string, body: any, args?: RequestInitJson): Promise<Response>;
  put<T>(url: string, body: any, args?: RequestInitJsonTransform<T>): Promise<T>;

  patch(url: string, body: any, args?: RequestInitJson): Promise<Response>;
  patch<T>(url: string, body: any, args?: RequestInitJsonTransform<T>): Promise<T>;

  delete(url: string, args?: RequestInit): Promise<Response>;
  delete<T>(url: string, args?: RequestInitTransform<T>): Promise<T>;

  observeStream(
    url: string,
    method: string,
    body: any,
    opts: RequestInitJson
  ): Observable<Uint8Array>;

  getStream(url: string, args?: RequestInit): Observable<Uint8Array>;
  postStream(url: string, body: any, args?: RequestInitJson): Observable<Uint8Array>;
  putStream(url: string, body: any, args?: RequestInitJson): Observable<Uint8Array>;
  patchStream(url: string, body: any, args?: RequestInitJson): Observable<Uint8Array>;
  deleteStream(url: string, args?: RequestInit): Observable<Uint8Array>;
}
