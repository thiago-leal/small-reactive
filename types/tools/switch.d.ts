import { Component } from "../../core";
import { Constructable } from "../utils/constructable";

export class Switch extends Component {
  get selected(): Component | undefined;
  get keys(): string[];

  setComponent(
    key: string,
    component: Component | Constructable<Component> | (() => Component)
  ): void;
  select(key: string): void;
  render(): string;
}
