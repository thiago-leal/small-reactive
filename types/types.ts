export type ValueOrPromise<T> = T | Promise<T>;

export type ValueOrArray<T> = T | T[];
