import { Module } from "../core/module";
import { Service } from "../core/service";
import { Constructable } from "./constructable";

export declare class Injector {
  constructor(
    getModule: () => Module
  );

  inject<T extends Service>(classRef: Constructable<T>): T;
  inject<T extends Service>(
    classRef: Constructable<T>, options: { optional: boolean }
  ): T | undefined;
  inject<T extends Service, U>(
    classRef: Constructable<T>, options: { optional: true, default: U }
  ): T | U;
}
