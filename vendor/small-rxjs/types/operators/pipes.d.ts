import { Observable, UnaryFunction } from "../observable";

export function map<T, R>(transform: (value: T, index?: number) => R): UnaryFunction<T, R>;
export function filter<T>(condition: (value: T, index?: number) => boolean): UnaryFunction<T, T>;
export function first<T>(): UnaryFunction<T, T>;
export function last<T>(): UnaryFunction<T, T>;
export function take<T>(limit: number): UnaryFunction<T, T>;
export function timeout<T>(time: number): UnaryFunction<T, T>;
export function keepAlive<T>(time: number): UnaryFunction<T, T>;
export function retry<T>(count: number): UnaryFunction<T, T>;
export function retry<T>(options: {
  count?:           number
  delay?:           number
  resetOnSuccess?:  boolean
}): UnaryFunction<T, T>;
export function finalize<T>(callback?: () => void): UnaryFunction<T, T>;
export function throwIf<T, R>(condition: (value: T) => R, error?: any): UnaryFunction<T, T>
export function catchError<T, U>(
  callback: (error: any, caught: Observable<T>) => Observable<U> | Promise<U> | U[]
): UnaryFunction<T, U>
export function switchMap<T, R>(
  transform: (value: T, index: number) => Promise<R> | Array<R> | Observable<R>
): UnaryFunction<T, R>
