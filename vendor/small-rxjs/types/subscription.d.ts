export interface Unsubscriber {
	unsubscribe(): void;
}

export class Subscription implements Unsubscriber {
	unsubscribe(): void;
	add(subscription: Unsubscriber): void;
	insertIn(subscription: Subscription): void;
}
